import numpy as np
import numpy.random as rd
import os
import sys
sys.path.append("../../libraries")
import lib
import drum
from importlib import reload
from keymap import var_gen
from pydub import AudioSegment
os.environ["IMAGEIO_FFMPEG_EXE"] = "/usr/bin/ffmpeg"
from moviepy.editor import VideoFileClip

reload(lib)
reload(drum)

lib.select_sf2("../../sf2/MuldjordKit")
lib.select_sf2("../../sf2/Black_Pearl_5-1.1")
#lib.select_sf2("../../sf2/Red_Zeppelin_5-1.1")
os.system("rm segments.pkl")
kmap = "Crocel"

myvar = locals()
var_gen(myvar, mmap=kmap)


def mil_save(gv=120):
    nm = "silence"
    t = 9
    for i in range(4):
        lib.sg_save(t*[0], [c] + 8*[n], t*[gv], t*[0], nm + " " + str(i))

    nm = "end"
    lib.sg_save(4*[Cs], 4*[n], 4*[gv], 4*[0], nm + " 0")
    lib.sg_save(4*[Rd], 4*[n], 4*[gv], 4*[0], nm + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[gv], 4*[0], nm + " 2")
    lib.sg_save(4*[Dr], 4*[n], 4*[gv-40], 4*[-15], nm + " 3")


def metal_beat_save(gv=100, ach=-80):
    nm = "metal beat "
    nb = 1
    lib.sg_save(8*[Hh], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, Sn, 0, Sn], 4*[n], 4*[gv], 4*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, 0, Dr, 0], 4*[n], 4*[gv], 4*[0], nm + str(nb) + " 3")

    nb = 2
    lib.sg_save(8*[Ho], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, Sn, 0, Sn], 4*[n], 4*[gv], 4*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, Dr, 0, 0, Dr, Dr, 0, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = "2b"
    lib.sg_save(8*[Cs], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, Sn, 0, Sn], 4*[n], 4*[gv], 4*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, Dr, 0, 0, Dr, Dr, 0, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")


    nb = 3
    lib.sg_save(8*[Cs], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, 0, Sn, 0, 0, 0, Sn, 0], 8*[c], 8*[gv+20], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, 0, 0, 0, 0, Dr, 0, 0], 8*[c], 8*[gv+10], 8*[0], nm + str(nb) + " 3")

    nb = 4
    lib.sg_save([Cs, 0, Cs, 0, Cs, 0, Cs, 0], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, 0, 0, 0, 0, 0, Sn, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, Dr, 0, 0, 0, 0, 0, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 5
    lib.sg_save(4*[Cs, 0], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, 0, Sn, 0, 0, 0, Sn, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, Dr, 0, Dr, Dr, Dr, 0, Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 6
    lib.sg_save(4*[Cs, 0], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, 0, 0, 0, Sn, 0, 0, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, Dr, 0, Dr, 0, 0, 0, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 7
    lib.sg_save(4*[Cs, 0], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, 0, Sn, 0, 0, 0, Sn, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save(8*[Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 8
    lib.sg_save(8*[Cs], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, 0, Sn, 0, 0, 0, Sn, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, 0, 0, 0, 0, Dr, 0, Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 9
    lib.sg_save(8*[Cs], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, Sn, 0, 0, 0, Sn, 0, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, 0, Dr, Dr, Dr, 0, Dr, Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 10
    lib.sg_save(8*[Ho], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save([0, 0, 0, 0, Sn, 0, 0, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[0], 4*[n], 4*[0], 4*[0], nm + str(nb) + " 2")
    lib.sg_save([Dr, Dr, 0, Dr, 0, 0, 0, Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")


def metal_fill_save(gv=100, ach=-80):
    nm = "metal fill "
    sil = [[0], [4*n]]
    nb = 1
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) +" 0")
    lib.sg_save(16*[Sn], 16*[cc], 16*[gv], 4*[-20, 0, 0, 0], nm + str(nb) + " 1")
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) + " 2")
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) + " 3")

    nb = 2
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) +" 0")
    lib.sg_save(4*[Sn, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[Tl, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 2")
    lib.sg_save(4*[0, Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 3
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) +" 0")
    lib.sg_save(2*[Sn, 0, 0, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(2*[0, Th, Tl, 0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 2")
    lib.sg_save(2*[0, 0, 0, Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 4
    lib.sg_save(8*[Ho], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save(8*[Sn], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(8*[0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 2")
    lib.sg_save(8*[Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 5
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) +" 0")
    lib.sg_save(3*[Sn, Sn, Sn, 0] + 4*[Sn], 16*[cc], 16*[gv], 4*[-20, 0, 0, 0], nm + str(nb) + " 1")
    lib.sg_save(3*[0, 0, 0, Tl]+4*[0], 16*[cc], 16*[gv], 4*[-20, 0, 0, 0], nm + str(nb) + " 2")
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) + " 3")

    nb = 6
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) +" 0")
    lib.sg_save(8*[Sn] + 8*[0], 16*[cc], 16*[gv], 4*[-20, 0, 0, 0], nm + str(nb) + " 1")
    lib.sg_save(8*[0]+8*[Tl], 16*[cc], 16*[gv], 4*[-20, 0, 0, 0], nm + str(nb) + " 2")
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) + " 3")

    nb = 7
    lib.sg_save(8*[0], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save(8*[45], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 1")
    lib.sg_save(8*[Th], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 2")
    lib.sg_save(8*[Dr], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 8
    lib.sg_save(8*[0], 8*[c], 8*[70], 8*[0], nm + str(nb) + " 0")
    lib.sg_save(4*[Th] + 8*[Sn], 4*[c]+8*[cc], 12*[gv], 12*[0], nm + str(nb) + " 1")
    lib.sg_save(4*[Tl] + 4*[0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 2")
    lib.sg_save(4*[Dr] + 4*[0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 9
    lib.sg_save(sil[0], sil[1], sil[0], sil[0], nm + str(nb) +" 0")
    lib.sg_save(2*[Sn, 0] + 5*[Sn], 4*[c] + 4*[cc] + [n], 9*[gv], 9*[0], nm + str(nb) + " 1")
    lib.sg_save(2*[Tl, 0] + 4*[0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 2")
    lib.sg_save(2*[0, Dr] + 4*[0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")

    nb = 10
    lib.sg_save(6*[Ho] + 2*[0], 8*[c], 8*[gv+ach], 8*[0], nm + str(nb) + " 0")
    lib.sg_save(6*[Sn] + 4*[Dr], 6*[c] + 4*[cc], 10*[gv], 10*[0], nm + str(nb) + " 1")
    lib.sg_save(8*[0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 2")
    lib.sg_save(6*[Dr] + 2*[0], 8*[c], 8*[gv], 8*[0], nm + str(nb) + " 3")


def save():
    mil_save()
    metal_beat_save()
    metal_fill_save()


def song(seed=None):
    if not seed:
        seed = rd.randint(100000)

    mel = {}
    mel = lib.mel_sgAdd("silence", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 2b", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 1", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 2", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 2", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 5", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 3", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 2", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 4", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 5", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 10", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 4", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 6", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 6", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 7", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 5", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 8", mel, atk_var=5, vol_var=5)
    for i in range(7):
        mel = lib.mel_sgAdd("metal beat 5", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("metal fill 9", mel, atk_var=5, vol_var=5)
    mel = lib.mel_sgAdd("silence", mel, atk_var=5, vol_var=5)

    name = "metal_" + str(seed)

    return mel, name


if __name__ == "__main__":
    save()
    mel, name = song()
    drum.mel2video(mel, 110, name, along="./Sds/metal.mp3", playing=0, verb=1,
                   kmap=kmap, C_DIR="../Drumeo/l2/", KIT_DIR="../../Ckit/")
    #clip = VideoFileClip("./Vids/" + name + ".mp4")
    #sub = clip.subclip(4, clip.end)
    #sub.write_videofile("./Vids/" + name + "c.mp4")


