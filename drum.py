from os.path import exists
from matplotlib import animation, transforms
import matplotlib.pyplot as plt
import matplotlib.image as img
from scipy import ndimage

import numpy.random as rd
import os
from math import pi, cos, sin

import numpy as np
from scipy.signal import savgol_filter
import matplotlib.image as Img
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from pydub import AudioSegment

from lib import mel2raind, MidiFile, mel_add, mid2aud, ibi2tpo, midi_bpm, mid2aud, play_along, playwav
from keymap import *
kmap = "Crocel"
myvar = locals()
var_gen(myvar, mmap=kmap)


def mel2mid(mel, tempo, tpb=960):
    mid = MidiFile()
    mid.ticks_per_beat = tpb
    mid = midi_bpm(mid, tempo)
    mid = mel_add(mid, mel)
    return mid


def fix_image(im_name, sd_name, vid_name):
    sl_audio = AudioSegment.silent(duration=0)
    audio = AudioSegment.from_file(sd_name)
    dur = audio.duration_seconds
    ds = 10
    Images = [ImageClip(im_name).set_duration(dur-ds)]
    Images += [ImageClip("./Ims/sub.png").set_duration(ds)]
    video = concatenate(Images, method="compose")
    audioclip = AudioFileClip(sd_name)
    video.audio = audioclip
    video.write_videofile("./Vids/" + vid_name + '.mp4', fps=1)


def gen_coordinates(diam, big, pos, cosinus=True, smooth=True):
    """Generate one dimension of the coordinates for the circle

    Parameters
    ----------
    pos: floats, coordinate of the circle
    crl: float, control how round the circle is
    smooth: Boolean, smooth or not the circle

    Return
    ------
    coor:1 dimension array of floats. Corresponding to x and y.
    """
    # The max is to avoid negative diameters
    nbpt = len(diam)
    if cosinus:
        coor = pos + big*(diam*np.array([cos(i) for i in np.linspace(0, 2*pi, nbpt)]))
    else:
        coor = pos + big*(diam*np.array([sin(i) for i in np.linspace(0, 2*pi, nbpt)]))
    if smooth:
        # Smooth the lines to have nice circles
        coor = savgol_filter(coor, 31, 10) # ndow size 11, polynomial order 3
    return coor


def cercle_coor(big=1, xpos=0, ypos=0, oval=1, nbpt=200, std=0.003):
    """Generate the coordinates of the circle

    Parameters
    ----------
    big: float, size of the circle
    (xpos, ypos): floats, coordinates of the circle

    Output
    ------
    shape: couple of floats which are the coordinates of the circle
    """
    diam = rd.normal(0.5, std, nbpt)
    x = gen_coordinates(diam, big*oval, xpos)
    y = gen_coordinates(diam, big, ypos, cosinus=False)
    return (x, y)


def drumkit(seed=None, kit_c=[0 for i in range(9)], ax=None):
    """Draw the drumkit
    """
    if not seed:
        seed = np.random.randint(10000)
    np.random.seed(seed)

    if not ax:
        fig, ax = plt.subplots(figsize=(18,9))
    ax.set_xlim(-9, 9)
    ax.set_ylim(-4.5, 4.5)

    pos = {"Dr":(1, 0, 0.2, 5), "Th":(1.5, -1.5, 2),
           "Hh":(2, -7, 1), "Tm":(2, 1.5, 2),
           "Tl":(3, 2, -2), "Cs":(2.5, -7, 4),
           "Rd":(3, 6.5, 2), "Sn":(2.5, -2, -2),
           "Cs2":(2.6, 7, 4)}
    coor = []
    kit = ["Dr", "Sn", "Th", "Tm", "Tl", "Hh", "Cs", "Rd", "Cs2"]
    for pos_key in kit:
        c_pos = pos[pos_key]
        if len(c_pos) == 4: #if the shape is not round
            coor.append(cercle_coor(c_pos[0], c_pos[1], c_pos[2], c_pos[3]))
        else:
            coor.append(cercle_coor(c_pos[0], c_pos[1], c_pos[2]))
    fills = []
    for i, c_coor in enumerate(coor):
        x, y = c_coor
        fills.append(ax.fill(x, y, lw=2, ec="black", fc=3*(0.99-kit_c[i],), clip_on=False)[0])
    adjust_spines(ax, [])
    return fig, fills







def adjust_spines(ax, spines):
    """removing the spines from a matplotlib graphics.
    taken from matplotlib gallery anonymous author.

    parameters
    ----------
    ax: a matplolib axes object
        handler of the object to work with
    spines: list of char
        location of the spines

    """
    for loc, spine in ax.spines.items():
        if loc in spines:
            pass
            # print 'skipped'
            # spine.set_position(('outward',10)) # outward by 10 points
            # spine.set_smart_bounds(true)
        else:
            spine.set_color('none')
            # don't draw spine
            # turn off ticks where there is no spine

    if 'left' in spines:
        ax.yaxis.set_ticks_position('left')
    else:
        # no yaxis ticks
        ax.yaxis.set_ticks([])

    if 'bottom' in spines:
        ax.xaxis.set_ticks_position('bottom')
    else:
        # no xaxis ticks
        ax.xaxis.set_ticks([])



def raind_unpack(raind, dur, kmap):
    """Turn a packed raind into a rain to put into a film
    """
    raind_u = np.zeros((9, dur))
    notes = [[Dr], [Sn, Ss], [Th], [Tm], [Tl], [Hh, Ho, Hp], [Cs], [Rd], [Cs2]]
    for c_raind in raind:
        for i, c_note in enumerate(notes):
            if c_raind[1] in c_note:
                raind_u[i, c_raind[0]] = c_raind[2]

    return raind_u


def tail(raind_u, tau=1.1):
    """Create a tail after a jump in vol"""
    for i in range(8):
        for j, c in enumerate(raind_u[i, :-1]):
            if raind_u[i, j+1] == 0:
                raind_u[i, j+1] = c/tau
    return raind_u


def mel2video(mel, tempo, name="default", tailv=1.6, along="", add=10, sub=15,
              playing=0,fps=24, verb=False, kmap="", KIT_DIR="../Ckit/"
              , C_DIR="../virtualdrumset/", inv=0):
    """Make a film from a rain
    """
    if not os.path.isdir("Vids"):
        os.system("mkdir Vids")
    if not os.path.isdir("Sds"):
        os.system("mkdir Sds")
    btw = 1000/fps
    mid = mel2mid(mel, tempo)
    if kmap == "AVL":
        aud = mid2aud(mid)
    else:
        mid.save(KIT_DIR + "gizmo.mid")
        os.chdir(KIT_DIR)
        os.system("drumgizmo -i midifile -I file=gizmo.mid,midimap=Midimap_full.xml,speed=2.0 -o wavfile -O file=prefix CrocellKit_full.xml")
        audR = AudioSegment.from_file("prefixAmbRight-1.wav")
        audL = AudioSegment.from_file("prefixAmbLeft-0.wav")
        aud = AudioSegment.from_mono_audiosegments(audR, audL)
        os.system("rm gizmo.mid")
        os.system("rm *.wav")
        os.chdir(C_DIR)


    if along != "":
        aud = play_along(aud, along, add, sub, inv)

    if playing:
        playwav(aud)
        return
    aud.export("./Sds/" + name + ".mp3")
    global ax, raind_u, fig, fills
    raind, t_max = mel2raind(mel, tempo)
    fig, ax = plt.subplots(figsize=(18,9))
    raind_u = raind_unpack(raind, t_max, kmap)
    raind_u = tail(raind_u, tailv)
    nframes = t_max
    fig, fills = drumkit()

    def animate(i):
        global fills, raind_u, fig
        [fills[j].set_fc(3*(0.99-raind_u[j, i],)) for j in range(9)]
        if verb:
            print(i/nframes)

        return ax

    anim = animation.FuncAnimation(fig, animate, frames=nframes, interval=btw)
    anim.save('./Vids/%sn.mp4' % name, fps=fps, extra_args=['-vcodec', 'libx264'])
    plt.close()
    os.system("ffmpeg -i ./Vids/%sn.mp4 -i ./Sds/%s.mp3 -c copy -map 0:v:0 -map 1:a:0 ./Vids/%s.mp4" %(name, name, name))
    #Clean
    os.system("rm ./Vids/%sn.mp4" %name)


def test_mel(seed=None, nit=32):
    if seed == None:
         seed = np.random.randint(100000)
    np.random.seed(seed)
    name = "Test_" + str(seed)
    mel = {}
    s = [0, 480, 0, (0, 0)]
    mel[0] = [[Ho, 240, int(np.random.randint(30)), (0, 0)] for i in range(2*nit)]
    mel[1] = int(nit/2)*([s, [Dr, 480, int(np.random.normal(90,10)), (0, 0)]])
    mel[2] = [[0, 480, 0, (0, 0)] for i in range(4)]
    mel[3] = int(nit/2)*([[Sn, 480, int(np.random.normal(90,10)), (0, 0)], s])
    return mel, name


if __name__ == "__main__":
    #fig, fills = drumkit()
    mel, name = test_mel()
    mel2video(mel, 80, name, tailv=1.6, playing=0, verb=True)


