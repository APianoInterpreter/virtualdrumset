import unittest
import matplotlib
from matplotlib import transforms
import matplotlib.image as img
from scipy import ndimage
import matplotlib.pyplot as plt
import numpy.random as rd
import numpy as np
import os
from shutil import copyfile
from pathlib import Path
import pickle

import pygame
from pydub import AudioSegment
from pydub.playback import play as playwav  # To be able to play the tune
import midi2audio as m2a
from mido import MetaMessage, Message, MidiFile, MidiTrack

#Generate an opart and turn it into a tracks plus set a tempo track
def part_multi(part):
    """Spli a part with multiple notes into multiple parts"""
    npart = 0
    for p in part:
        npart = max(npart, len(p[0].split(" "))) # Determine the number of parts
    return npart

def gen_opart(notes, dur, vol, atk={}):
    """"Merge the different element to add a track to a midi file"""
    if (len(notes) != len(dur)) or (len(notes) != len(vol)):
        print(len(notes), len(dur), len(vol))

    l_size = len(notes)
    if not atk:
        atk = l_size*[(0, 0)]
    out = [[notes[i], dur[i], vol[i], atk[i]] for i in range(l_size)]
    return out


def char2note(char):
    """Convert a char to a midi note number"""
    if char=="s":
        return 0
    notes = {'a': 69, 'b': 71, 'c': 60, 'd': 62, 'e': 64, 'f': 65, 'g': 67}
    note_nb = notes[char[0]]
    note_nb += (int(char[1])-4) * 12
    if len(char) == 3:  # Add the accent if there is one
        if char[2] == '-':
            note_nb -= 1
        if char[2] == '+':
            note_nb += 1
    return note_nb


def add2trk(trk, note, dur, vol, channel=0, on=True):
    """Add a note_on or note_off message to a track"""
    if isinstance(note, str):
        note = char2note(note)

    if on:
        trk.append(Message("note_on",
                           channel=channel,
                           note=note,
                           velocity=vol,
                           time=dur))
    else:
        trk.append(Message("note_off",
                           channel=channel,
                           note=note,
                           velocity=vol,
                           time=dur))
    return trk



def opart2trk(opart, tinit=100, char=True, instr=16):
    """Turn an opart into a midi track"""
    trk = MidiTrack()

    if char:
        for i, c_note in enumerate(opart):
            if c_note[0] == "s": # Develop the note if there is a silence
                opart[i] = [0, c_note[1], 0, (0, 0)]
            else:
                c_note[0] = char2note(c_note[0])

    note, dur, vol, atk = opart[0]
    trk = add2trk(trk, note, tinit+atk[0], vol)

    nb_notes = len(opart)
    flag = False
    fin = 0
    for i in range(1, nb_notes):
        if not flag:
            dur_off = 0
        note_c, dur_c, vol_c, atk_c = opart[i-1]
        note_n, dur_n, vol_n, atk_n= opart[i]
        if atk_c[1] <= atk_n[0]:
            trk = add2trk(trk, note_c, dur_c + atk_c[1] - atk_c[0] - dur_off, 100, on=False)
            trk = add2trk(trk, note_n, atk_n[0] - atk_c[1], vol_n)
            fin = atk_n[1] - atk_n[0]
            flag = False

        else:
            dur_off = atk_c[1] - atk_n[0]
            dur_on = dur_c - atk_c[1] + atk_n[0]
            trk = add2trk(trk, note_n, dur_on , vol_n)
            trk = add2trk(trk, note_c, dur_off , 100, on=False)
            fin = atk_c[0] + atk_c[1] - atk_n[0] + atk_n[1]
            flag = True

    note, dur, vol, atk = opart[-1]
    trk = add2trk(trk, note, dur+fin, 100, on=False)
    return trk


def part2trk(part):
    """Create a track from notes with a particular syntax."""
    trk = MidiTrack()  # Create a track

    for note in part:  # Enumerate each chord in the part
        dur = note[1]

        if note[0]=="s":
            n_nb = 0
            vol = 0
        else:
            if isinstance(note[0], str):
                n_nb = char2note(note[0])
            else:
                n_nb = note[0]
            vol = int(note[2])

        trk.append(Message("note_on", note=n_nb, velocity=vol,
                           time=0)) # Start pressing the note of the chord
        trk.append(Message("note_off", note=n_nb, velocity=100,
                           time=dur))

    return trk


def set_tempo(mid, times, bt=480):
    """Create a track setting a new tempo every bt ticks and add it to a midi"""
    trk = MidiTrack()
    trk.name = "Tempo variation"
    trk.append(MetaMessage("set_tempo",
                           tempo=times[0],
                           time=0))

    for i, time in enumerate(times[1:]):
        trk.append(MetaMessage("set_tempo",
                               time=bt,
                               tempo=time))  # Change the tempo every bt ticks

    mid.tracks.append(trk)
    return mid


def midi_bpm(mid, bpm):
    """Create a track setting the tempo in bpm"""
    trk = MidiTrack()
    trk.name = "Metadata"
    trk.append(MetaMessage("set_tempo",
                           tempo=tpo2ibi(bpm),
                           time=0))
    mid.tracks.append(trk)
    return mid



def rectify_atk(notes, atk):
    """Remove overlap betwween similar notes
    Comments
    --------
     Maybe add the dur in the future.
    """
    for i, c_note in enumerate(notes[:-1]):
        n_note = notes[i+1]
        c_atk = atk[i][1]
        n_atk = atk[i+1][0]
        if c_note==n_note and c_atk>n_atk:
            atk[i][1]=n_atk
    return atk


def set_pedal(mid, times, rand=True):
    """Create a pedal track on and off at given times"""
    trk = MidiTrack()
    trk.name = "Pedal"
    flip_on = True
    for t in times:
        if rand:
            p_on = rd.randint(64, 127)
            p_off = rd.randint(0, 64)
        else:
            p_on = 65
            p_off = 63
        if flip_on:
            trk.append(Message("control_change", control=64, value=p_on,
                               time=t))
            flip_on = False
        else:
            trk.append(Message("control_change", control=64, value=p_off,
                               time=t))
            flip_on = True
    mid.tracks.append(trk)
    return mid


def ibi2tpo(ibs):
    """Turn interbeat interval into a tempo or the other ways around"""
    return (1000000/ibs)*60


def tpo2ibi(tpo):
    """Turn interbeat interval into a tempo or the other ways around"""
    return int(np.round((1000000*60)/tpo))



#Tools to generate and randomize the different values of an opart
def crescendo(begin, end, nel, inv=False):
    """Return a list starting at begin and finishing at end of nel elements following a log increase or decrease if decre is true
    """
    if not inv:
        ar = np.logspace(np.log10(begin), np.log10(end), nel, base=10)
        return [int(i) for i in ar]
    else:
        ar = np.logspace(np.log10(end), np.log10(begin), nel, base=10)
        return [int(i) for i in ar]


def normal_bd(mean=0, var=1, bounds=(-20, 20)):
    """Return a int bounded random number following a normal distribution"""
    low, high = bounds
    out = low - 1
    while out <= low or out >= high:  # Go in a loop until you fall within boundsa. It creates some distortion but nothing major
        out = np.round(rd.normal(loc=mean, scale=var))
    return int(out)


def vol_rand(vol, var=5, bounds=(0, 127)):
    """"Randomize a list of velocities"""
    out_vol = []
    for c_vol in vol:
        r_vol = [normal_bd(c_vol, var, bounds)]
        out_vol.append(int(r_vol[0]))
    return out_vol


def atk_rand(atk, var=10):
    """Randomize the atk for the drum"""
    if len(atk) == 1:
        return [(atk[0], 0)]
    atk_rd0 = int(rd.normal(atk[0], var))
    atk_rd1 = int(rd.normal(atk[1], var))
    out = [(atk_rd0, atk_rd1)]
    for c_atk in atk[1:]:
        atk_rd2 = int(rd.normal(c_atk, var))
        out.append((atk_rd1, atk_rd2))
        atk_rd1 = int(atk_rd2)
    return out


def atk_rand_c(atk, notes, var=10, bound=[10, 10]):
    """"Randomize a list of attack"""
    out_atk = []
    for c_atk in atk:
        couple = [normal_bd(c_atk[0], var, (c_atk[0]-bound[0], c_atk[0]+bound[1]))]
        couple += [normal_bd(c_atk[1], var, (c_atk[1]-bound[0], c_atk[1]+bound[1]))]
        out_atk.append(couple)
    # Avoid scenarios of an identical note overlap
    for i, c_note in enumerate(notes[:-1]):
        if c_note == notes[i+1]:
            out_atk[i][1] = out_atk[i+1][0] - 20
    return out_atk


def randomize_ibi(ibi, var=1):
    """"Randomize a list of ibi"""
    out_ibi = []
    for c_ibi in ibi:
        r_ibi = [normal_bd(c_ibi, var, (0, 5000000))]
        out_ibi.append(int(r_ibi[0]))
    return out_ibi



# Functions used to play the midi file
def play(midi_filename):
  '''Stream music_file in a blocking manner'''
  import pygame
  set_pygame()
  clock = pygame.time.Clock()
  pygame.mixer.music.load(midi_filename)
  pygame.mixer.music.play()
  while pygame.mixer.music.get_busy():
    clock.tick(30) # check if playback has finished


def set_pygame():
    # mixer config
    freq = 44100  # audio CD quality
    bitsize = -16   # unsigned 16 bit
    channels = 2  # 1 is mono, 2 is stereo
    buffer = 1024   # number of samples
    pygame.mixer.init(freq, bitsize, channels, buffer)

    # optional volume 0 to 1.0
    pygame.mixer.music.set_volume(0.8)


#Tools to render a midi file into a wav
def select_sf2(name):
    """Select an sf2 used to play a piece"""
    home = str(Path.home())
    if os.path.exists(home + "/.fluidsynth/default_sound_font.sf2"):
        os.remove(home + "/.fluidsynth/default_sound_font.sf2")
    copyfile(name + ".sf2", home + "/.fluidsynth/default_sound_font.sf2")


def mid2aud(mid, name=None):
    """Create AudioSegment from a Mid file"""
    mid.save("temp.mid")
    if name:
        m2a.FluidSynth().midi_to_audio('temp.mid', name)
        seg = None
    else:
        m2a.FluidSynth().midi_to_audio('temp.mid', 'temp.wav')
        seg = AudioSegment.from_file('temp.wav')
    os.remove("temp.mid")
    if not name:
        os.remove("temp.wav")
    return seg




#Part for making thumbnails
def random_walk(seed, nstp, loc=0, scale=1, bounds=(0, 127)):
    """Generate a random walk following a normal law

    Parameters
    ----------
    seed: int or float, initial value of the random walk
    nstp: int, number of elements in the sequence
    loc: float, the mean value of the gaussian noise
    scale: float, the standard deviation

    Output
    ------
    rands: ints list, the entire sequence between low and high of size nstp
    """
    if bounds:
        low, high = bounds
    else:
        low, high = seed-20000, seed+20000
    rands = [seed]

    for i in range(nstp-1):
        seed += rd.normal(loc, scale)  # Generate the next value using a normal law

        # The two bounds are sticky seed cannot go beyond them.
        if seed < low:
            rands += [low]
            seed = low
        elif seed > high:
            rands += [high]
            seed = high
        else:
            rands += [seed]  # Add the int to the list


    return rands


def adjust_spines(ax, spines):
    """removing the spines from a matplotlib graphics."""
    for loc, spine in ax.spines.items():
        if loc in spines:
            pass
            # print 'skipped'
            # spine.set_position(('outward',10)) # outward by 10 points
            # spine.set_smart_bounds(true)
        else:
            spine.set_color('none')
            # don't draw spine
            # turn off ticks where there is no spine

    if 'left' in spines:
        ax.yaxis.set_ticks_position('left')
    else:
        # no yaxis ticks
        ax.yaxis.set_ticks([])

    if 'bottom' in spines:
        ax.xaxis.set_ticks_position('bottom')
    else:
        # no xaxis ticks
        ax.xaxis.set_ticks([])


def rainbow_text(fig, x,y,ls,lc,**kw):
    """Color the text"""

    t = plt.gca().transData

    for s,c in zip(ls,lc):
        text = plt.text(x,y,s,color=c, transform=t, **kw)
        text.draw(fig.canvas.get_renderer())
        ex = text.get_window_extent()
        t = transforms.offset_copy(text._transform, x=ex.width, units='dots')


def thumbnail(fname, comp, nb, end=False, ytb=True):
    if ytb:
        fig, ax = plt.subplots(figsize=(13.3333, 7.5))
        xrainb = 350
        tsize = 50
    else:
        fig, ax = plt.subplots(figsize=(11.25, 11.25))
        xrainb = 300
        tsize = 40
    plt.imshow(img.imread("./Ims/blurred_drop%s.png" %(nb)))
    adjust_spines(ax, [])
    rainbow_text(fig, xrainb , 52, ['R','AI','Ndom'], ['black', 'red', 'black'] , weight="bold", size=60, family='poppins')
    xcor = 610
    if end:
        ax.text(xcor, 392, 'Please', color='black' , weight="bold", size=60, family='poppins', ha='center', multialignment="center")
        ax.text(xcor, 502, 'like', color='red' , weight="bold", size=60, family='poppins', ha='center', multialignment="center")
        ax.text(xcor, 612, 'subscribe', color='black' , weight="bold", size=60, family='poppins', ha='center', multialignment="center")
        ax.text(xcor, 722, 'and share', color='black' , weight="bold", size=60, family='poppins', ha='center', multialignment="center")
    else:
        ax.text(xcor, 400, fname, color='black' , weight="bold", size=tsize, family='poppins', ha='center', multialignment="center")
        ax.text(xcor, 550, 'by', color='black' , weight="light", size=30, family='poppins', ha='center', multialignment="center")
        ax.text(xcor, 650, comp, color='black' , weight="light", size=30, family='poppins', ha='center', multialignment="center")
    s = 200000
    plt.plot(np.linspace(220, 1000, s), random_walk(122, s, scale=0.01, bounds=(120, 122)), lw=1, color="black")
    if ytb:
        plt.savefig("./Ims/temp.png")
    else:
        plt.savefig("./Ims/insta.png")

    if end:
        os.system('inkscape --export-png=credit.png --export-dpi=300 ./Ims/frame.svg')
    else:
        os.system('inkscape --export-png=./Ims/Youtube.png --export-dpi=300 ./Ims/frame.svg')

    # Clean the folder
    # os.system('rm ./Ims/temp.png')



def thumbnailbw(name, nb, end=False, ytb=True):
    if ytb:
        fig, ax = plt.subplots(figsize=(13.3333, 7.5))
    else:
        fig, ax = plt.subplots(figsize=(11.25, 11.25))
    plt.imshow(img.imread("./Ims/blurred_drop%s.png" %(nb)), cmap="gray")
    adjust_spines(ax, [])
    # Build a rectangle in axes coords
    left, width = .25, .5
    bottom, height = .25, .5
    right = left + width
    top = bottom + height
    ax.text(0.5 * (left + right), 0.85, "RAiN Drum",
            weight="light", size=60, family='Poppins',
            horizontalalignment='center',
            verticalalignment='center',
            transform=ax.transAxes)
    ax.text(0.5 * (left + right), 0.5 * (bottom + top), name,
            weight="light", size=60, family='Poppins',
            horizontalalignment='center',
            verticalalignment='center',
            transform=ax.transAxes)
    s = 200000
    plt.plot(np.linspace(390, 930, s), random_walk(201, s, scale=0.01, bounds=(200, 202)), lw=1, color="black")
    if ytb:
        plt.savefig("./Ims/ytb.png")
    else:
        plt.savefig("./Ims/insta.png")

    # Clean the folder
    # os.system('rm ./Ims/temp.png')


#Elements necessary to generate rain videos
def duration_ticks(trk):
    """Give the total duration in ticks of a tracks"""
    time = 0
    for msg in trk:
        time += msg.time
    return time


def opart_split(opart, tinit=480):
    """Split an opart in two part to include the silence before andafter notes"""
    part0 = [["s", tinit]]
    for i in range(0, len(opart), 2):
        part0.append(list(opart[i][:3]))
        try:
            part0.append(["s", opart[i+1][1]])
        except:
            continue
            #part0.append(list(opart[-1][:3]))

    part1 = [["s", tinit]]
    for i in range(1, len(opart)+1, 2):
        try:
            part1.append(["s", opart[i-1][1]])
            part1.append(list(opart[i][:3]))
        except:
            continue
            #part1.append(["s", opart[-1][1]])
    return part0, part1




def ibs2ticks(nticks, ibs, bt, ticks_per_bt=480):
    """Return a list of how long a tick last in microsecods

    Parameters
    ----------
    nticks: int, total number of ticks in the song
    ibs: float, duration in microseconds
    bt: the regular interval when the tempo is changing
    """
    ticks = [0 for i in range(nticks)]  # Build list of ticks

    if len(ibs) < len(ticks):
        nb_ibs = int(len(ticks)/bt)
        ibs = [ibs[0] for i in range(nb_ibs)]

    for i in range(int(len(ticks)/bt)):
        for j in range(bt):
            ticks[i*bt + j] = ibs[i]/ticks_per_bt
    return ticks


def part2rain(ibs, part, bt, ticks_per_bt=480, rain=None, rangec=None, echo=3, color="viridis", fps=24, pos=None):
    """Convert a sheet of music into a rain file: Rain format {(xpos, ypos, time): (color, big_init, duration)"""
    pin, pout = 2, 8
    keys, dur, vol = [], [], []
    r_tim, r_dur, r_vol = [], [], []
    ticks_prog, time, key_dur = 0, 0, 0
    ticks = ibs2ticks(duration_ticks(part2trk(part)), ibs, bt, ticks_per_bt) #+tinit

    if not rangec: # By default span the entire colorrange
        mink = 0
        maxk = 127
    if not rain: # If there is not preexisting rain
        rain = {}
    if color != "black": # Case where the rain is not uniformly black
        cmap = matplotlib.cm.get_cmap(color) # determine the range of the keys
        mink, maxk = rangec
        rangek = maxk-mink
        rangec = np.linspace(0, 1, rangek, endpoint=True)


    for i, p in enumerate(part): #Loop through all elements of a part
        if p[0] != 0 and p[0] != "s": #Case for silences
            keys.append(p[0])
            dur.append(p[1])
            vol.append(p[2])
        else:
            keys.append(p[0])
            dur.append(p[1])
            vol.append(0)

        # This is the heart of the function
        r_tim += [int(((time + key_dur)/1000000)*fps)]
        key_dur = np.sum(ticks[ticks_prog:ticks_prog + dur[i]])
        r_dur += [int(key_dur/1000000*fps*echo)]
        r_vol += [vol[i]/64]
        time = np.sum(ticks[:ticks_prog])
        ticks_prog += dur[i]

        if p[0]== 0 or p[0] == 's':  # Silences are not drops
             continue

        if color != "black":
            if p[0] is str:
                rgba = cmap(rangec[char2note(p[0])-mink-1])
            else:
                rgba = cmap(rangec[p[0]-mink-1])
        else:
            rgba = (0, 0, 0)

        if pos == None:
            rain[(rd.uniform(-9, 9), rd.uniform(-9, 9), r_tim[i])] = (rgba, r_vol[i], r_dur[i])
        elif pos == "left":
            rain[(rd.uniform(-9, 0), rd.uniform(-4, 9), r_tim[i])] = (rgba, r_vol[i], r_dur[i])
        elif pos == "right":
            rain[(rd.uniform(0, 9), rd.uniform(-4, 9), r_tim[i])] = (rgba, r_vol[i], r_dur[i])
        elif pos == "down":
            rain[(rd.uniform(-9, 9), rd.uniform(-9, -4), r_tim[i])] = (rgba, r_vol[i], r_dur[i])
        elif pos == "center":
            rain[(0, 0, r_tim[i])] = (rgba, r_vol[i], r_dur[i])
        elif pos == "ur":
            rain[(rd.uniform(pin, pout), rd.uniform(pin, pout), r_tim[i])] = (rgba, r_vol[i], r_dur[i])
        elif pos == "ul":
            rain[(rd.uniform(-pout, -pin), rd.uniform(pin, pout), r_tim[i])] = (rgba, r_vol[i], r_dur[i])
        elif pos == "dr":
            rain[(rd.uniform(pin, pout), rd.uniform(-pout, -pin), r_tim[i])] = (rgba, r_vol[i], r_dur[i])
        elif pos == "dl":
            rain[(rd.uniform(-pout, -pin), rd.uniform(-pout, -pin), r_tim[i])] = (rgba, r_vol[i], r_dur[i])

    return rain


def part2raind(ibs, part, ticks_per_bt=960, raind=None, fps=24):
    """Subfunction to raind"""
    keys, dur, vol, r_tim, r_vol = [], [], [], [], []
    ticks_prog, time, key_dur = 0, 0, 0
    tim = duration_ticks(part2trk(part))
    ticks = ibs2ticks(tim, ibs, 480, ticks_per_bt)

    for i, p in enumerate(part): #Loop through all elements of a part
        key = p[0]
        dur = p[1]
        if key != 0:
            vol = p[2]

        # This is the heart of the function
        r_tim += [int(((time + key_dur)/1000000)*fps)]
        key_dur = np.sum(ticks[ticks_prog:ticks_prog + dur])
        time = np.sum(ticks[:ticks_prog])
        ticks_prog += dur

        if key != 0:
            raind.append([r_tim[i], key, vol/128])
    max_time = int(((time + key_dur)/1000000)*fps)

    return raind, max_time


def mel2raind(mel, tempo, ticks_per_beat=960, raind=None):
    """Generate the raind from the melody"""
    if not raind:
        raind = []

    parts = []
    for i in range(4):
        raind, t_max = part2raind([tpo2ibi(tempo)], mel[i], raind=raind)

    return raind, t_max


def gen_coordinates(nbpt, diam=1, cosinus=True):
    """Generate one dimension of the coordinates for the circle

    Parameters
    ----------
    nbpt: float, definition of the circle
    diam: float, diameter of the circle
    cosinus: bool, output the x or y coordinates the circle

    Return
    ------
    coor:1 dimension array of floats. Corresponding to x or y.
    """
    diam = np.array([diam for i in range(nbpt)])
    if cosinus:
        coor = diam*np.array([cos(i) for i in np.linspace(0, 2*pi, nbpt)])
    else:
        coor = diam*np.array([sin(i) for i in np.linspace(0, 2*pi, nbpt)])
    return coor


def cercle_coor(nbpt, diam=1):
    """Generate the coordinates of the circle Parameters nbpt: float, definition of the circle . diam: float, diameter of the circle Outputshape: couple of floats which are the coordinates of the circle"""
    x = gen_coordinates(nbpt, diam=diam)
    y = gen_coordinates(nbpt, diam=diam, cosinus=False)
    return (y, x)


def list2pos(lst, max_loc=10):
    """Turn a list into a series of position given a max number of position"""
    loc = 0
    pos = []
    for i, c_lst in enumerate(lst[:-1]):
        pos += (lst[i+1] - c_lst)*[loc]
        loc += 1
        if loc >= max_loc:
            loc = 0
    return pos


def clock(ibs, nticks, bt, ticks_pr_bt=480, jump=3, npos=150):
    """Create a rain corresponding to a rotating clock"""

    time_ticks = ibs2ticks(nticks, ibs, bt, ticks_pr_bt)
    jump_occur = []
    for i in range(int(np.ceil(nticks/jump))):
        jump_dur = np.sum(time_ticks[:i*jump])
        jump_occur += [int(jump_dur/100000*24)]

    pos = list2pos(jump_occur, npos)
    ccoor = cercle_coor(npos, 3)
    for i in range(len(pos)):
        pos[i] = (ccoor[0][pos[i]], ccoor[1][pos[i]])
    return pos

    return pos



#Saving and loading to add drum riffs
def sg_save(notes, dur, vol, atk, name, fname="segments.pkl"):
    """Save a segment in a file
    Parameters
    ----------
    Return
    ------
    """

    sgs = {}
    if not os.path.isfile(fname):
        with open(fname,'wb') as file:
            pickle.dump(sgs, file)

    with open(fname, 'rb') as file:
        sgs = pickle.load(file)
        rec_dict = [notes, dur, vol, atk]
        sgs[name] = rec_dict

    with open(fname, 'wb') as file:
        pickle.dump(sgs, file)
    return "Saved segment"


def sg_load(name, fname="segments.pkl"):
    """Load a segment as 4-uplet lists"""
    with open(fname, 'rb') as handle:
        sgs = pickle.load(handle)
    return sgs[name]


#Including a melody with multiple channels in a mid
def mel_sgAdd(name, melody={}, tempo=[], atk_var=0, vol_var=1, add_i=4):
    """Include a beat measure in a melody, for now you have 3 different styles of beats"""
    if not melody:
        for i in range(add_i):
            melody[i] = []

    for i in range(add_i):
        notes, dur, vol, atk = sg_load(name + " " + str(i))
        vol = vol_rand(vol, var=vol_var)
        if not isinstance(atk[0], int):
            atk = atk_rand_c(atk, notes, var=atk_var)
        else:
            atk = atk_rand(atk, var=atk_var)
        melody[i] += gen_opart(notes, dur, vol, atk)

    if tempo:
        return melody, tempo + tempo_add
    else:
        return melody


def mel_add(mid, mel, raining=0, ibs=[]):
    c = 480
    n = 960 # Dirty bug fix
    rain = {}
    for ch in mel.keys():
        trk = opart2trk(mel[ch], char=False)
        mid.tracks.append(trk)
        if raining:
            pos = ["ul", "dr", "ur", "dl"]
            for part in opart_split(mel[ch]):
                rain = part2rain(ibs, part, c, n, rain=rain, color="black", pos=pos[ch-1])
    if raining:
        return mid, rain
    else:
        return mid


def play_along(aud, name, aud_add=10, song_sub=15, inverse=True):
    """Overlay a play along to the battery"""
    aud = aud + aud_add
    song = AudioSegment.from_file(name)
    song = song - song_sub
    if inverse:
        aud = song.overlay(aud)
    else:
        aud = aud.overlay(song)
    return aud


class tests(unittest.TestCase):
    def test_char2note(self):
        char = 'c4'
        out = char2note('c4')
        exp_out = 60
        self.assertEqual(out, exp_out)


    def test_gap(self):
        opart = [[65, 480, 100, (-10, -200)],
                 [63, 200, 100, (15, 10)]]
        out = opart2trk(opart, char=False)
        exp_out = MidiTrack()
        exp_out += [Message('note_on', note=65, time=90, velocity=100)]
        exp_out += [Message('note_off', note=65, time=290, velocity=100)]
        exp_out += [Message('note_on', note=63, time=215, velocity=100)]
        exp_out += [Message('note_off', note=63, time=195, velocity=100)]
        try:
            self.assertEqual(out, exp_out)
        except AssertionError:
            print("gap")
            for i, c_out in enumerate(out):
                print(exp_out[i].time, c_out.time)

    def test_over(self):
        opart = [[65, 500, 100, (-50, -100)],
                 [63, 600, 100, (-250, -300)]]
        out = opart2trk(opart, char=False)
        exp_out = MidiTrack()
        exp_out += [Message('note_on', note=65, time=50, velocity=100)]
        exp_out += [Message('note_on', note=63, time=350, velocity=100)]
        exp_out += [Message('note_off', note=65, time=150, velocity=100)]
        exp_out += [Message('note_off', note=63, time=400, velocity=100)]
        try:
            self.assertEqual(out, exp_out)
        except AssertionError:
            print("overlap")
            for i, c_out in enumerate(out):
                print(exp_out[i].time, c_out.time)

        #atk = [(0, -10), (-11, 11), (10, 0), (51, -52)]
         #cor_atk = [(0, -12), (-11, 9), (10, 0), (48, -52)]

    def test_opart2trk(self):
        opart = [['c4', 480, 60, (0, 0)],
                 ['d4', 480, 60, (-180, -180)],
                 ['e4', 480, 60, (0, 0)]]
        exp_out = MidiTrack()
        exp_out += [Message('note_on', note=60, time=100, velocity=60)]
        exp_out += [Message('note_on', note=62, time=300, velocity=60)]
        exp_out += [Message('note_off', note=60, time=180, velocity=100)]
        exp_out += [Message('note_off', note=62, time=300, velocity=100)]
        exp_out += [Message('note_on', note=64, time=180, velocity=60)]
        exp_out += [Message('note_off', note=64, time=480, velocity=100)]
        out = opart2trk(opart)
        try:
            self.assertEqual(out, exp_out)
        except AssertionError:
            for i, c_out in enumerate(out):
                print(exp_out[i], c_out)

    def test_rectify_atk(self):
        notes = ["a4", "a4"]
        atk = [[0, -50], [-70, 0]]
        out = rectify_atk(notes, atk)
        exp_out = [[0, -70], [-70, 0]]
        try:
            self.assertEqual(out, exp_out)
        except AssertionError:
            print(out, exp_out)

    def test_ib(self):
        nticks = 2*480
        ibs = [500000, 600000] + 6*[600000] # The addition avoid index error
        bt = 120
        out = ibs2ticks(nticks, ibs, bt)

        exp_out = 120*[500000/480]
        exp_out += (7*120)*[600000/480]

        self.assertEqual(out, exp_out)

    def test_clock():
        part = [['c4', 480], ['d4', 480]]
        ibs = [500000, 600000] + 6*[600000] # The addition avoid index error
        bt = 120
        out = clock(part, ibs, bt)

    def test_list2pos(self):
        lst = [0, 10, 20, 25, 32]
        max_pos = 3
        out = list2pos(lst, max_pos)
        exp_out = 10*[0] + 10*[1] + 10*[2] + 5*[0] + 7*[1]
        self.assertEqual(list(out), list(exp_out))

    def test_part_multi(self):
        part = [["c4 d4", "480 240"],
                ["s", "480"],
                ["c4 d4 e4", 480]]
        out = part_multi(part)
        exp_out = 3
        parts = []
        parts += [["c4", 480], ["s", 480], ["c4", 480]]
        parts += [["d4", 240], ["s", 480+240], ["d4", 480]]
        parts += [["s", 480], ["s", 480], ["e4", 480]]
        self.assertEqual(out, exp_out)


if __name__ == "__main__":
    #print(atk_rand([-10, 25, 0, -10]))
    test = tests()
    test.test_gap()
    test.test_over()
    test.test_opart2trk()
    test.test_rectify_atk()
    #sg_save([0], [960], [0], [(0, 0)], "4onF Tom")
    #sg_save(8*[42], 8*[240], 8*[70], [(0, 0)], "4onF Hihat")
    test.test_part_multi()
    mel = {}
    s = [0, 480, 0]
    mel[0] = [[42, 480, int(np.random.normal(60,10))] for i in range(4)]
    mel[1] = 2*([s, [38, 480, int(np.random.normal(60,10))]])
    mel[2] = [[0, 480, 0] for i in range(4)]
    mel[3] = 2*([[36, 480, int(np.random.normal(60,10))], s])
    raind, t_max = mel2raind(mel, 60)
    print(raind)
