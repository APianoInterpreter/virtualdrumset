def var_gen(myVar, tb=960, mmap="AVL"):
    # Drum keys
    if mmap == "AVL":
        midmap = {"Hh":42, "Ho":46, "Hp":44, "Rd":53, "Cs":49, "Sn":38,
                  "Ss":37, "Th":61, "Tm":62, "Tl":63, "Dr":36, "Cs2":57}
    elif mmap == "Crocel":
        midmap = {"Dr":35, "Ss":37, "Sn":38, "Hh":42, "Hp":47, "Ho":46,
                  "Cs":49, "Cs2":57, "Th":48, "Tm":47, "Tl":43, "Rd":51}

    for k in midmap.keys():
        myVar[k] = midmap[k]

    # Piano keys
    scale = ["c", "cs", "d", "ds", "e", "f", "fs", "g", "gs", "a", "as", "b"]
    gamme = ["do", "dod", "re", "red", "mi", "fa", "fad", "sol", "sold", "la", "lad", "si"]
    for i in range(12):
        key = scale[i]
        key2 = gamme[i]
        for j in range(9):
            oc = str(j)
            myVar[key + oc] = i + (j+1)*12
            myVar[key2 + oc] = i + (j+1)*12

    # Durations
    dur = [tb, tb/2, tb/4, tb/6, tb/3, (tb/2)/3, (tb/4)/3, 20]
    dur = [int(i) for i in dur]
    fname = ['n', 'c', 'cc', 'ccc', 'tn', 'tc', 'tcc', 'f']
    ename = ['q', 'e', 's', 't', 'tq', 'te', 'ts', 'f']
    for i in range(len(dur)):
        myVar[fname[i]] = dur[i]
        myVar[ename[i]] = dur[i]


